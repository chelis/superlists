from django.conf.urls import patterns, url
from .views import persona_login

urlpatterns = patterns('',
                       url(r'^login$', persona_login, name='persona_login'),
                       url(r'^logout$', 'django.contrib.auth.views.logout',
                           {'next_page': '/'}, name='logout'),
                       )

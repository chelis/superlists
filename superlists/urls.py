from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
                       # Examples:
                       url(r'^$', 'lists.views.home_page', name='home_page'),
                       url(r'^lists/', include('lists.urls')),
                       url(r'^accounts/', include('accounts.urls')),
                       # url(r'^blog/', include('blog.urls')),

                       # url(r'^admin/', include(admin.site.urls)),
                       )
